﻿using ADOSample.DAL.Entities;
using ADOSample.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOSample.Console
{
    class Program
    {
        static CategoryRepository CatRepo = new CategoryRepository();
        static void Main(string[] args)
        {
            bool exit = false;
            while (!exit)
            {
                System.Console.Clear();
                int choice = DisplayMenu();
                System.Console.Clear();
                switch (choice)
                {
                    case 1:
                        DisplayCategories();
                        break;
                    case 2:
                        InsertCategory();
                        DisplayCategories();
                        break;
                    case 3:
                        DisplayCategories();
                        DeleteCategory();
                        System.Console.Clear();
                        DisplayCategories();
                        break;

                    case 4:
                        DisplayCategories();
                        UpdateCategory();
                        System.Console.Clear();
                        DisplayCategories();
                        break;

                    case 42:
                        exit = true;
                        break;
                    default:
                        System.Console.WriteLine("404 Not Found");
                        break;
                }
                System.Console.ReadKey();
            }
        }

        static void UpdateCategory()
        {
            int id;
            do
            {
                System.Console.WriteLine("Quelle categorie voulez modifier (id)?");
            } while (!int.TryParse(System.Console.ReadLine(), out id));
            Category cat = CatRepo.Get(id);
            if (cat != null)
            {
                System.Console.WriteLine($"Ancien nom: {cat.Name}");
                System.Console.WriteLine("Comment voulez vous renommer la categorie?");
                string newName = System.Console.ReadLine();
                cat.Name = newName;
                if (CatRepo.Update(cat))
                {
                    System.Console.WriteLine($"La categorie n°{id} a été modifié");
                }
                else
                {
                    System.Console.WriteLine("l'élément n'a pu pu être modifié");
                } 
            }
            else
            {
                System.Console.WriteLine("l'élément demandé n'existe pas");
            }
            System.Console.ReadKey();
        }

        static void DeleteCategory()
        {
            int id;
            do
            {
                System.Console.WriteLine("Quelle category voulez supprimer (id)?");
            } while (!int.TryParse(System.Console.ReadLine(), out id));
            if (CatRepo.Delete(id))
            {
                System.Console.WriteLine($"La categorie n°{id} a été supprimée");
            }
            else
            {
                System.Console.WriteLine("l'élément n'a pu pu être supprimé");
            }
            System.Console.ReadKey();
        }

        static int DisplayMenu()
        {
            int choice;
            do
            {
                System.Console.WriteLine("1. Afficher toutes les categories");
                System.Console.WriteLine("2. Créer une nouvelle categorie");
                System.Console.WriteLine("3. Supprimer categorie");
                System.Console.WriteLine("4. Mettre à jour une categorie");
            } while (!int.TryParse(System.Console.ReadLine(), out choice));
            return choice;
        }

        static void DisplayCategories()
        {
            System.Console.WriteLine("Categories");
            System.Console.WriteLine("==========");
            IEnumerable<Category> cats = CatRepo.GetAll();
            foreach (Category item in cats)
            {
                System.Console.WriteLine($"{item.Id} {item.Name}");
            }
        }

        static void InsertCategory()
        {
            System.Console.WriteLine("Entrez le nom de la categorie à ajouter");
            string entree = System.Console.ReadLine();
            Category c = new Category();
            c.Name = entree;
            CatRepo.Insert(c);
        }
    }
}
