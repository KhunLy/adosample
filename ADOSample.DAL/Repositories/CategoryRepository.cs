﻿using ADOSample.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOSample.DAL.Repositories
{
    public class CategoryRepository : IRepository<Category>
    {
        private string _ConnectionString 
            = @"Data Source=WAD-PCPROF\ADMINSQL;Initial Catalog=ADOSample;Integrated Security=True";

        public Category Get(int id)
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();
                string query = "SELECT * FROM Category WHERE Id = @id";
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Category c = new Category();
                    c.Id = (int)reader["Id"];
                    c.Name = reader["Name"] as string;
                    return c;
                }
                return null;
            }
        }

        public IEnumerable<Category> GetAll()
        {
            // je cree une variable qui condiendra la connection
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();
                string query = "SELECT * FROM Category";
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = query;
                SqlDataReader reader = cmd.ExecuteReader();
                List<Category> result = new List<Category>();
                while (reader.Read())
                {
                    Category c = new Category();
                    c.Id = (int)reader["Id"];
                    c.Name = reader["Name"] as string;
                    //yield return c;
                    result.Add(c);
                }
                return result;
            }
            // A la fin du using la methode "dispose" de ma connection
            // libère les ressources non managées
        }

        public int Insert(Category item)
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText 
                    = "INSERT INTO Category(Name) OUTPUT inserted.Id VALUES(@name)";
                cmd.Parameters.AddWithValue("name",item.Name);
                return (int)cmd.ExecuteScalar();
                
            }
        }

        public bool Update(Category item)
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE Category SET Name = @name WHERE Id = @id";
                cmd.Parameters.AddWithValue("id", item.Id);
                cmd.Parameters.AddWithValue("name", item.Name);
                return cmd.ExecuteNonQuery() != 0;
            }
        }

        public bool Delete(int id)
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM Category WHERE Id = @id";
                cmd.Parameters.AddWithValue("id", id);
                return cmd.ExecuteNonQuery() != 0;
            }
        }
    }
}
